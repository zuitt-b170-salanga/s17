console.log("hello world");

/*
	How do we display the ff tasks in the console?


	1. drink html
	2. eat javascript
	3. inhale css
	4. bake bootstrap

	take a screenshot of your output and send it in the google chat	 
*/

// console.log("1. drink html");
// console.log("2. eat javascript");
// console.log("3. inhale css");
// console.log("4. bake bootstrap");
// ******** ++OUTPUT++ ********
// 	    1. drink html 
//  	2. eat javascript
//  	3. inhale css 
//  	4. bake bootstrap
// ============================================ 



/*
	ARRAYS - are used to store multiple related data values inside a single variable. Arrays are 
	declared using the square brackets([])
	SYNTAX:
			let/const <arrayName> = ["elementA", "elementB",... "elementN",];

	ARRAYS are used if there is a need to manipulate the related values stored in it

	INDEX - position of each element in the array. Calling an element through the index is 
		determined by using this syntax
			
			arrayName[index]

	REMINDER: the index alwayys starts w/ 0. Counting the elements inside would start w/ 0 
		instead of 1.
		formula: -nth element -1/arrayName.length-1
				 -start counting fr 0
*/


let tasks = ["eat javascript", "drink html", "inhale css", "bake bootstrap"];
console.log(tasks);
// ******** ++OUTPUT++ ********
// (4)	['eat javascript', 'drink html', 'inhale css', 'bake bootstrap']
// 		0: "eat javascript"
// 		1: "drink html"
// 		2: "inhale css"
// 		3: "bake bootstrap"
// 		length: 4
// ============================================ 

//getting the element through the array's index
console.log(tasks[2]);
/* let indexOfLastElement = tasks.length -1; 
	console.log(indexOfLastElement); */
// ******** ++OUTPUT++ ********
// 			inhale css
// ============================================ 


//getting the number of elements
console.log(tasks.length);
// ******** ++OUTPUT++ ********
// 			4
// ============================================ 

//trying to access the index of a non-existing element would result to undefined
console.log(tasks[4]);
// ******** ++OUTPUT++ ********
// 			undefined
// -------------------------------------------------



// @@@@@@@@@@@@@@@@@@@@@@@  Array Manipulation  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


// I. MANIPULATING THE END OF THE ARRAY 

// a)ADDING AN ELEMENT
	let numbers = ["one", "two", "three", "four"];
	console.log(numbers);
// ******* ++OUTPUT++ **********
// (4)['one', 'two', 'three', 'four']
// =============================
//console.log(numbers[4]); =====> undefined; 0-3 indices only

//		1. Using the Assignment Operator
	numbers[4]="five";
	console.log(numbers);
// ******* ++OUTPUT++ **********
// (5)['one', 'two', 'three', 'four', 'five']
// =============================

//		2. Push Method
//			------ adds (an) element/s at the end of the array
	numbers.push("element");
	console.log(numbers);
// ******* ++OUTPUT++ **********
// (6)['one', 'two', 'three', 'four', 'five', 'element']
// =============================

//  	3. Push Method Using Callback Function
// 			** Callback Function -----> a function that is passed on (or inserted) to another function. 
//				this is done bec the inserted function is ff a particular syntax & the developer is trying
//				to simplify that syntax by just inserting it inside another function
	function pushMethod(element){
		numbers.push(element)
	}
	pushMethod('six');
	pushMethod('seven');
	pushMethod('eight');
	console.log(numbers);
// ******* ++OUTPUT++ **********
// (9)['one', 'two', 'three', 'four', 'five', 'element', 'six', 'seven', 'eight']
// =============================


// b) REMOVING AN ELEMENT
// 		1. Pop Method - removes the element at the end of the array (last element)
	numbers.pop();  // for less data inside
	console.log(numbers);
// ******* ++OUTPUT++ **********
// (8)['one', 'two', 'three', 'four', 'five', 'element', 'six', 'seven']
// =============================

	function popMethod(){  //for efficiency
	numbers.pop();
	}
	popMethod();
	console.log(numbers);
// ******* ++OUTPUT++ **********
// (7) ['one', 'two', 'three', 'four', 'five', 'element', 'six']
// =============================



// -----------------------
// II. MANIPULATING THE BEGINNING/START OF AN ARRAY 

// 	a) Shift Method ( REMOVE THE 1st ELEMENT )
numbers.shift();
console.log(numbers);
// ******* ++OUTPUT++ **********
// (6)['two', 'three', 'four', 'five', 'element', 'six']
// =============================

// callback function
function shiftMethod(){ //() is blank 
	numbers.shift(); // will remove 1st element only so no need to indicate parameter
}
shiftMethod();
console.log(numbers);
// ******* ++OUTPUT++ **********
// (5)['three', 'four', 'five', 'element', 'six']
// =============================






//........ continue 106
// -----------------------------
//	b) Unshift Method (ADDING AN ELEMENT)
numbers.unshift("zero");
console.log(numbers);

// callback function
function unshiftMethod(element){   // element parameter must be the same for lines 112&113
	numbers.unshift(element);      // 113--- placeholder element
}
unshiftMethod("mcdo");  // 
/* unshiftMethod("jollibee");
unshiftMethod("1"); */
console.log(numbers);


// // III. Arrangement of the elements


// let numbs = [16,27,32,12,6,8,236]
// console.log(numbs);
// // a) sort method - arranges the elements in ascending or descending order. It has an anonymous 
// 		//function inside that has 2 parameters  
// 			// anonymous function - unnamed function & can only be used once
// 				/*
// 					2 parameters inside the anonymous functtion represents:
// 						1st parameter - 1st/smallest/starting element
// 						2nd parameter - last/biggest/ending element
// 				*/
// /*
// 	SYNTAX:
// 		arrayName.sort(
// 			function(a,b){
// 				a-b - ascending
// 				b-a - descending
// 			}
// 		)
// */
// numbs.sort(
// 	function(a,b){
// 		return a-b
// 	}
// )
// console.log(numbs);

// // descending order
// numbs.sort(
// 	function(a,b){
// 		return b-a
// 	} 
// );
// console.log(numbs);

// // b) reverse method - reverses the order of the elements in an array. It will depend on the last
// //	arrangement of the array, regardless if it is ascending, descending in random order
// numbs.reverse();
// console.log(numbs);


// // ============================
// // c) splice method - ctrl x + ctrl p
// /*
// 	- directly manipulates the array
// 	- 1st parameter - the index of the element fr w/c the omitting will begin
// 	- 2nd parameter - determines the number of elements to be omitted
// 	- 3rd parameter onwards - the replacements for the removed elements
// 	SYNTAX:
// 		let/const<newArray>
// */
// // one parameter: (pure omission)
// // let nums = numbs.splice(1);
// // two parameters:(pure omission)
// // let nums = numbs.splice(0,2);
// // three parameters: replacements
// let nums = numbs.splice(4,2,31,11,111);  
// console.log(numbs);
// console.log(nums);

// // let nums = numbs.splice(1);  // will omit fr index 1 to end 
// // console.log(numbs);
// // console.log(nums);


// // ===========================
// // d) slice method  - ctrl c + ctrl p
// /*
// 	-does not affect the original array; creates a sub-array, but does not omit any element fr 
// 		the orig array
// 	-1st parameter - index where copying will begin
// 	-2nd paramente - the no. of ..........
// */
// //one parameter
// //let slicedNums = the no. of elements to be copied starting fr the 1st element (copying will
// //	still begin)
// let slicedNums = numbs.slice(2,7); // index 2 up to 7th element
// console.log(numbs);
// console.log(slicedNums);
// // (8) [6, 8, 12, 16, 31, 11, 111, 236]
// // (5) [12, 16, 31, 11, 111]


// // ===================
// //IV.   merging of array
// // a) Concat
// console.log(numbers);
// console.log(numbs);
// let animals = ["dog", "tiger", "kangaroo", "chicken"];
// console.log(animals);

// let newConcat = numbers.concat(numbs, animals);
// console.log(newConcat);
// console.log(numbers);
// console.log(numbs);
// console.log(animals);

// // b) join method -
// //parameters -
// let meal = ["rice", "steak", "juice"];
// console.log(meal)

// let newJoin = meal.join()
// console.log(newJoin);

// newJoin = meal.join("")
// console.log(newJoin);

// newJoin = meal.join(" ")
// console.log(newJoin);

// newJoin = meal.join("-")
// console.log(newJoin);

// //toString method
// console.log(nums);
// console.log(typeof nums[3]);

// let newString = numbs.toString();
// console.log(newString);
// console.log(typeof newString);

// /*Accessors*/
// let countries = ["US", "PH", "JP", "HK", "SG","PH", "NZ"];
// // indexOf - returns the 1st index it finds fr the beginning of the array
// /*
// 	SYNTAX:
// 	arrayName.indexOf()
// */
// let index = countries.indexOf("PH")
// console.log(index);
// //finding a non-existing element - returns -1
// index = countries.indexOf("AU");
// console.log(index);

// // lastIndexOf() - finds the index of the element starting fr the end of the array
// /*
// 	SYNTAX:
// 	arrayName.lastIndexOf("PH");
// */
// index = countries.lastIndexOf("PH");
// console.log(index);

// //returns -1 for non-existing elements
// index = countries.lastIndexOf("Ph");
// console.log(index);


// /*  .....
// if (countries.indexOf("CAN") === -1) {
// 	console.log("Element not existing");
// }else{
// 	console.log("Element exists in the array");
// }
// */


// //Iterators
// let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
// console.log(days)

// // forEach - returns each element in the array
// /*
// 	array.forEach(
// 		function(element){
// 			statement/s
// 		}

// 	)
// */
// days.forEach(
// 		function(element){
// 			console.log(element)
// 		}
// 	)


// // map
// 	/*
		
// 	*/
// 	//returns a copy of an array fr the orig w/c can be manipulated
// 	/*
		
// 	*/
// let mapDays = days.map(
// 		function(element){
// 			return `${element} is the day of the week.`
// 		}
// 	)
// 	console.log(mapDays);
// 	console.log(days);

// // filter - filters the elements & copies them into another array
// console.log (numbs);
// let newFilter = numbs.filter(
// 		function (element) {
// 			return element < 30;
// 		}
// )
// console.log(newFilter);
// console.log(numbs);

// //includes - returns true (boolean) if the element/s are inside the array

// let animalIncludes = animals.includes("dog");
// console.log(animalIncludes);

// //every - checks if all the element pass the condition (returns true if all of them pass)
// console.log(nums);
// let newEvery = nums.every(
// 	function(element){
// 		return(element>20);
// 	}
// )
// console.log(newEvery);

// // some
// let newSome = nums.some(
// 	function(element){
// 		return(element>30)
// 	}
// )
// console.log(newSome);
// //OUTPUT==> true


// nums.push(50)
// console.log(nums);
// //reduce - performs the operation in all of the elements in the array
// //1st parameter - last element
// let newReduce = nums.reduce(
// 	function(a,b){
// 		//return a+b
// 		return b-a
// 	}
// )
// console.log(newReduce);

// let average = newReduce/nums.length
// console.log (average)

// //toFixed -
// console.log(average.toFixed(2));

// /*
	

// */
// console.log(parseInt(average.toFixed(2)));
// console.log(parseFloat(average.toFixed(2)));

